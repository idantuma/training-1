package application;


import application.ui.MenuGenerator;

import java.util.Scanner;

public class ClubManagementDriver {
    public static final Scanner keyboardReader = new Scanner(System.in);

    public static void main(String[] args){
        MenuGenerator menuGenerator = new MenuGenerator();
        String userInput = "";
        int previousDepth;

        do{
            previousDepth = menuGenerator.getDepth();
            System.out.println(menuGenerator.displayMenu());
            System.out.print("Select: ");
            userInput = keyboardReader.nextLine();
            System.out.println();
            menuGenerator.selectItem(Integer.parseInt(userInput));

        }while ((previousDepth!= 0) || (!userInput.equals("0")));

        System.out.println("Terminating Program. Goodbye!");

    }

}
