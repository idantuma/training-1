package application.ui;

import application.ClubManagementDriver;
import entity.club.CommunityMember;

import java.util.HashMap;
import java.util.Map;

public class MemberManagement {
    private String email, name, phone, nickname, birthday;

    private Map<String, CommunityMember> memberList;

    public MemberManagement(){
        memberList = new HashMap<>();
    }

    private void search(){
        while (true){
            System.out.print("\tMember's email: ");
            email = ClubManagementDriver.keyboardReader.nextLine();

            if(email.equals("0")){
                break;
            }

            if(memberList.containsKey(email)){
                System.out.println("\t\tFound member: " + memberList.get(email) + "\n");
                break;
            }
            else {
                System.out.println("\t\tNo such a member with email: " + email + "\n");
            }
        }
    }

    public void register(){
        CommunityMember temp = null;
        while (true){
            System.out.print("\tNew member's email (0. Member menu): ");
            email = ClubManagementDriver.keyboardReader.nextLine();
            if(email.equals("0")){
                break;
            }
            System.out.print("\nname: ");
            name = ClubManagementDriver.keyboardReader.nextLine();

            System.out.print("\nphone number: ");
            phone = ClubManagementDriver.keyboardReader.nextLine();

            System.out.print("\nnickname: ");
            nickname = ClubManagementDriver.keyboardReader.nextLine();

            System.out.println("\nbirthday(yyyy.mm.dd): ");
            birthday = ClubManagementDriver.keyboardReader.nextLine();

            try {
                temp = new CommunityMember(email, name, phone);
            }catch (Exception e){
                System.out.println(e.getMessage());
            }

            if(temp != null){
                temp.setNickName(nickname);
                temp.setBirthDay(birthday);
                memberList.put(email, temp);
                System.out.println("\tRegistered member: " + temp.toString());
            }
        }
    }

    public void find(){
        do{
            search();
        }while (!name.equals("0"));
    }

    public void modify(){
        CommunityMember temp;

        while (true){
            search();
            if(email.equals("0")){
                break;
            }

            temp = memberList.get(email);

            System.out.print("\tNew name (Enter. no change): ");
            name = ClubManagementDriver.keyboardReader.nextLine();

            System.out.print("\tNew phone number (Enter. no change): ");
            phone = ClubManagementDriver.keyboardReader.nextLine();

            System.out.print("\tNew nickname (Enter. no change): ");
            nickname = ClubManagementDriver.keyboardReader.nextLine();

            System.out.print("\tNew birthday (yyyy.mm.dd) (Enter. no change): ");
            birthday = ClubManagementDriver.keyboardReader.nextLine();

            if(!name.equals("")){
                temp.setName(name);
            }

            if(!phone.equals("")){
                temp.setPhoneNumber(phone);
            }

            if(!nickname.equals("")){
                temp.setNickName(nickname);
            }

            if(!birthday.equals("")){
                temp.setBirthDay(birthday);
            }

            memberList.remove(email);
            memberList.put(email, temp);

            System.out.println("\t\tModified member: " + memberList.get(email) + "\n");
        }
    }

    //Todo: finish function definition

    public void remove(){
        String userResponse = "";

        search();

        if(email.equals("0")){
            return;
        }


    }
}
