package application.ui;

import application.ui.menu_items.MainMenu;
import application.ui.menu_items.MenuItem;

public class MenuGenerator {
    private StringBuilder menuBuilder;
    private MenuItem menu;
    int menuItemCounter;

    public MenuGenerator(){
        menu = new MainMenu();
    }

    public String displayMenu(){
        menuBuilder = new StringBuilder();
        menuItemCounter = 1;
        menuBuilder.append("..............................\n");
        menuBuilder.append(menu.getMenuTitle() + "\n");
        menuBuilder.append("..............................\n");
        for(String menuItem : menu.getMenuItems()){
            menuBuilder.append(menuItemCounter++ + ". " + menuItem + "\n");
        }
        menuBuilder.append("..............................\n");
        if(menu.hasExtraMenuItems()){
            for(String menuItem : menu.getExtraMenuItems()){
                menuBuilder.append(menuItemCounter++ + ". " + menuItem + "\n");
            }
            menuBuilder.append("..............................\n");
        }
        menuBuilder.append("0. " + menu.getReturnNav() + "\n");
        menuBuilder.append("..............................\n");

        return menuBuilder.toString();
    }

    public void selectItem(int selectedItem){
        menu.selectItem(selectedItem);
    }

    public String getInvalidInputMessage(){
        return "Input format is invalid. Please try again.";
    }

    public int getDepth(){
        return menu.getDepth();
    }
}
