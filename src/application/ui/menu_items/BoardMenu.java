package application.ui.menu_items;

public class BoardMenu extends MenuItem {
    public BoardMenu(){
        super();

        menuTitle = "Board menu";

        menuItems.add("Register a board");
        menuItems.add("Find boards by name");
        menuItems.add("Modify a board");
        menuItems.add("Remove a board");

        extraMenuItems.add("Posting Menu");

        returnNav = "Previous";
    }

    @Override
    public void selectItem(int selectedItem){
        switch (selectedItem){
            case 0:
                subMenu = null;
                break;
            case 5:
                addSubMenu();
                break;
            default:
                break;
        }
    }

    @Override
    public void addSubMenu(){
        subMenu = new PostingMenu();
    }
}
