package application.ui.menu_items;

public class PostingMenu extends MenuItem {
    public PostingMenu(){
        super();

        menuTitle = "Posting menu";

        menuItems.add("Find a board");
        menuItems.add("Register a posting");
        menuItems.add("Find postings in the board");
        menuItems.add("Find a posting");
        menuItems.add("Modify a posting");
        menuItems.add("Remove a posting");

        returnNav = "Previous";
    }

    @Override
    public void selectItem(int selectedItem){

    }
}
