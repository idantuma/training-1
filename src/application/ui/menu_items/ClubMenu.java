package application.ui.menu_items;

import application.ui.ComponentContainer;

public class ClubMenu extends MenuItem {
    ComponentContainer uiHelper;

    public ClubMenu(){
        super();

        uiHelper = ComponentContainer.getInstance();

        menuTitle = "Club menu";

        menuItems.add("Register");
        menuItems.add("Find");
        menuItems.add("Modify");
        menuItems.add("Remove");

        extraMenuItems.add("Membership menu");

        returnNav = "Previous";
    }

    @Override
    public void selectItem(int selectedItem){
        switch (selectedItem){
            case 0:
                subMenu = null;
                break;
            case 1:
                if(subMenu != null){
                    subMenu.selectItem(selectedItem);
                }
                else {
                    uiHelper.registerClub();
                }
                break;
            case 2:
                if(subMenu != null){
                    subMenu.selectItem(selectedItem);
                }
                else {
                    uiHelper.findClub();
                }
                break;
            case 3:
                if(subMenu != null){
                    subMenu.selectItem(selectedItem);
                }
                else {
                    uiHelper.modifyClub();
                }
                break;
            case 4:
                if(subMenu != null){
                    subMenu.selectItem(selectedItem);
                }
                else {
                    uiHelper.removeClub();
                }
                break;
            case 5:
                addSubMenu();
                break;
            default:
                break;
        }
    }

    @Override
    public void addSubMenu(){
        subMenu = new MembershipMenu();
    }
}
