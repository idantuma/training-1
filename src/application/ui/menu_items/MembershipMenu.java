package application.ui.menu_items;

import application.ui.ComponentContainer;

public class MembershipMenu extends MenuItem {
    ComponentContainer uiHelper;

    public MembershipMenu(){
         super();

         uiHelper = ComponentContainer.getInstance();

         menuTitle = "Membership menu";

         menuItems.add("Find a club");
         menuItems.add("Add a membership");
         menuItems.add("Find a membership");
         menuItems.add("Modify a membership");
         menuItems.add("Remove a membership");

         returnNav = "Previous";
    }

    @Override
    public void selectItem(int selectedItem){
        String temp;
        switch (selectedItem){
            case 0:
                subMenu = null;
                break;
            case 1:
                if(subMenu == null){
                    temp = uiHelper.setMembershipTargetClub();
                    if(!temp.equals("")){
                        menuTitle += " for [" + temp + "]";
                    }
                }
                else {
                    subMenu.selectItem(selectedItem);
                }
                break;
            case 2:
                if(subMenu == null){

                }
                else {
                    subMenu.selectItem(selectedItem);
                }
                break;
            default:
                break;
        }
    }
}
