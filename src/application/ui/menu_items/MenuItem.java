package application.ui.menu_items;

import java.util.ArrayList;
import java.util.List;

public class MenuItem {
    protected String menuTitle;
    protected List<String> menuItems;
    protected List<String> extraMenuItems;
    protected String returnNav;
    protected MenuItem subMenu;

    public MenuItem(){
        menuItems = new ArrayList<>();
        extraMenuItems = new ArrayList<>();
    }

    public int getDepth(){
        if(subMenu == null){
            return 0;
        }
        else {
            return subMenu.getDepth() + 1;
        }
    }

    public String getMenuTitle(){
        if(getDepth() > 0){
            return subMenu.getMenuTitle();
        }
        else {
            return menuTitle;
        }
    }

    public List<String> getMenuItems(){
        if(getDepth() > 0){
            return subMenu.getMenuItems();
        }
        else {
            return menuItems;
        }
    }

    public boolean hasExtraMenuItems(){
        if(getDepth() > 0){
            return subMenu.hasExtraMenuItems();
        }
        else {
            if(extraMenuItems.size() > 0){
                return true;
            }
            else {
                return false;
            }
        }
    }

    public String getReturnNav(){
        if(getDepth() > 0){
            return subMenu.getReturnNav();
        }
        else {
            return returnNav;
        }
    }

    public List<String> getExtraMenuItems(){
        if(getDepth() > 0){
            return subMenu.getExtraMenuItems();
        }
        else {
            return extraMenuItems;
        }
    }

    public void selectItem(int selectedItem){
        if(getDepth() > 0){
            if(selectedItem > 0){
                subMenu.selectItem(selectedItem);
            }
            else if(selectedItem == 0){
                if(getDepth() > 1){
                    subMenu.selectItem(selectedItem);
                }
                else if(getDepth() == 1){
                    subMenu = null;
                }
            }
        }
        else if(getDepth() == 0){
            if(selectedItem > 0){
                addSubMenu(selectedItem);
            }
        }
    }

    protected void addSubMenu(){
        subMenu = new MenuItem();
    }

    protected void addSubMenu(int menuIdx){
        subMenu = new MenuItem();
    }
}
