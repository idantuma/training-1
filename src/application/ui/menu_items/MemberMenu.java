package application.ui.menu_items;

public class MemberMenu extends MenuItem {
    public MemberMenu(){
        super();

        menuTitle = "Member menu";

        menuItems.add("Register");
        menuItems.add("Find");
        menuItems.add("Modify");
        menuItems.add("Remove");

        returnNav = "Previous";
    }

    @Override
    public void selectItem(int selectedItem){

    }
}
