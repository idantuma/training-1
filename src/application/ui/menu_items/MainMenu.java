package application.ui.menu_items;

public class MainMenu extends MenuItem {

    public MainMenu(){
        super();

        menuTitle = "Main menu";

        menuItems.add("Club menu");
        menuItems.add("Member menu");
        menuItems.add("Board menu");

        returnNav = "Exit program";
    }


    @Override
    public void addSubMenu(int menuIdx){
        switch (menuIdx){
            case 1:
                subMenu = new ClubMenu();
                break;
            case 2:
                subMenu = new MemberMenu();
                break;
            case 3:
                subMenu = new BoardMenu();
                break;
            default:
                break;
        }
    }
}
