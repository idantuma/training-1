package application.ui;

import application.ClubManagementDriver;
import entity.club.TravelClub;

import java.util.HashMap;
import java.util.Map;

public class ClubManagement {
    private static final int MAX_ID_LENGTH = 5;

    private String name;
    private String intro;
    private Map<String, TravelClub> travelClubs;

    public ClubManagement(){
        travelClubs = new HashMap<>();
    }

    private void search(){
        while (true){
            System.out.print("\tClub name to find (0. Club menu): ");
            name = ClubManagementDriver.keyboardReader.nextLine();
            if(name.equals("0")){
                break;
            }
            if(travelClubs.containsKey(name)){
                System.out.println("\t\t> Found club: " + travelClubs.get(name) + "\n");
                break;
            }
            else {
                System.out.println("\t\tNo such a club with name: " + name + "\n");
            }
        }
    }

    public void register(){
        name = "";
        intro = "";
        String id = "";
        String shortId;
        TravelClub temp;
        while(true){
            System.out.print("\tClub name (0. Club menu): ");
            name = ClubManagementDriver.keyboardReader.nextLine();
            if(name.equals("0")){
                break;
            }
            System.out.print("\tClub intro (0. Club name): ");
            intro = ClubManagementDriver.keyboardReader.nextLine();
            if(intro.equals("0")){
                break;
            }
            temp = new TravelClub(name, intro);

            shortId = String.valueOf(travelClubs.size() + 1);
            while(id.length() < (MAX_ID_LENGTH - shortId.length())){
                id += "0";
            }
            id = id.concat(shortId);
            temp.setAutoId(id);

            travelClubs.put(name, temp);
            System.out.println("\t\tRegistered club: " + temp + "\n");
        }
    }

    public void find(){
        do{
            search();
        }while (!name.equals("0"));
    }

    public void modify(){
        String newName = "";
        name = "";
        intro = "";
        TravelClub temp;

        while(true) {
            search();
            if(name.equals("0")){
                break;
            }

            temp = travelClubs.get(name);

            System.out.print("\tNew club name (0. Club menu, Enter. no change): ");
            newName = ClubManagementDriver.keyboardReader.nextLine();

            System.out.print("\tNew club intro (Enter. no change): ");
            intro = ClubManagementDriver.keyboardReader.nextLine();

            if(newName.equals("0")){
                break;
            }
            else {
                if (!newName.equals("")) {
                    temp.setName(newName);
                }
                else {
                    newName = name;
                }
            }

            if(!intro.equals("")){
                temp.setIntro(intro);
            }

            travelClubs.remove(name);
            travelClubs.put(newName,temp);

            System.out.println("\t\t> Modified club: " + travelClubs.get(newName) + "\n");
        }
    }

    public void remove(){
        name = "";
        String userResponse = "";

        search();

        if(name.equals("0")){
            return;
        }

        System.out.print("Remove this club? (Y:yes, N:no): ");
        userResponse = ClubManagementDriver.keyboardReader.nextLine().toUpperCase();

        if(userResponse.equals("Y")){
            travelClubs.remove(name);
            System.out.println("Removing a club --> " + name + "\n");
        }
        else {
            System.out.println("Remove cancelled, your club is safe --> " + name + "\n");
        }
    }

    public TravelClub retrieveClub(){
        search();
        if(name.equals("0")){
            return null;
        }
        else {
            return travelClubs.get(name);
        }
    }
}
