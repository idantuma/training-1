package application.ui;

import entity.club.TravelClub;

public class ComponentContainer {
    private static ClubManagement club = new ClubManagement();
    private static MembershipManagement membership = new MembershipManagement();

    private ComponentContainer(){}

    private static class ComponentContainerHolder{
        private static final ComponentContainer INSTANCE = new ComponentContainer();
    }

    public static ComponentContainer getInstance(){
        return ComponentContainerHolder.INSTANCE;
    }

    public void registerClub(){
        club.register();
    }

    public void findClub(){
        club.find();
    }

    public void modifyClub(){
        club.modify();
    }

    public void removeClub(){
        club.remove();
    }

    public String setMembershipTargetClub(){
        TravelClub targetClub = club.retrieveClub();

        membership.setTargetClub(targetClub);
        return targetClub.getName();
    }
}
